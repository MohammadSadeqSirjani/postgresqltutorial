## What is PostgreSQL?
PostgreSQL is a general purpose and object-relational database management system.
PostgreSQL was designed to run on UNIX-like platforms. However, 
PostgreSQL was then also designed to be portable so that it could run on various platforms such as Mac OS X, Solaris, and Windows.
PostgreSQL is free and open source software. Its source code is available under PostgreSQL license, 
a liberal open source license. You are free to use, modify and distribute PostgreSQL in any form.

## PostgreSQL features highlights
PostgreSQL has many advanced features that other enterprise database management systems offer, such as:
1.  User-defined types
2. Table inheritance
3. Sophisticated locking mechanism
4. Foreign key referential integrity
5. Views, rules, subquery
6. Nested transactions (savepoints)
7. Multi-version concurrency control (MVCC)
8. Asynchronous replication

## What makes PostgreSQL stand out
PostgreSQL is the first database management system that implements multi-version concurrency control (MVCC) feature, even before Oracle. The MVCC feature is known as snapshot isolation in Oracle.
PostgreSQL is a general-purpose object-relational database management system. It allows you to add custom functions developed using different programming languages such as C/C++, Java, etc.
PostgreSQL is designed to be extensible. In PostgreSQL, you can define your own data types, index types, functional languages, etc. If you don’t like any part of the system, you can always develop a custom plugin to enhance it to meet your requirements e.g., adding a new optimizer.
If you need any support, an active community is available to help. You can always find the answers from the PostgreSQL’s community for the issues that you may have when working with PostgreSQL. Many companies offer commercial support services in case you need one.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)