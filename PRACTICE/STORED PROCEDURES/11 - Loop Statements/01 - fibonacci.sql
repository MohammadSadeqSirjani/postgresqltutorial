/*
    LOOP
    -------
    <<label>>
    LOOP
        Statements;
        EXIT [<<label>>] WHEN condition;
    END LOOP;
    -------
*/

CREATE OR REPLACE FUNCTION "public"."fibonacci"("n" int4)
  RETURNS "pg_catalog"."int4" AS $BODY$
DECLARE 
	counter INTEGER = 0;
	i INTEGER = 0;
	j INTEGER = 1;	
BEGIN

	IF n < 1 THEN
		RETURN 0;
	END IF;
	
	LOOP
		EXIT WHEN counter = n;
		counter = counter + 1;
		SELECT j, j + i INTO i, j; 	
	END LOOP;
	
	RETURN j;
END; $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100

/*
    WHILE LOOP
    -------
    <<label>>
    WHILE condition LOOP
        expressions
    END LOOP;
    -------
*/

CREATE OR REPLACE FUNCTION fibonacci2(n INTEGER)
RETURNS INTEGER
AS $$ 
DECLARE 
	counter INTEGER = 0;
	i INTEGER = 0;
	J INTEGER = 1;
BEGIN 
	IF n < 1 THEN
		RETURN 0;
	END	IF;
	
	WHILE counter = n LOOP
		counter = counter + 1;
		SELECT j, j + i INTO i, j;
	END LOOP;
	
	RETURN j;
END; $$
LANGUAGE PLPGSQL;

/*
    FOR LOOP STATEMENT
    -------
    [<<label>>]
    FOR loop_counter IN [REVERSE] from..to [BY STATEMENT] LOOP
        loop_expression
    END LOOP [<<label>>];    
    -------
*/

-- 01
DO $$
BEGIN
	FOR counter IN 1..5 LOOP
		RAISE NOTICE 'COUNTER: %', counter;
	END LOOP;
END; $$

-- 02
DO $$
BEGIN
	FOR counter IN REVERSE 5..1 LOOP
		RAISE NOTICE 'COUNTER: %', counter;
	END LOOP;
END; $$

-- 03
DO $$
BEGIN
	FOR counter IN 1..6 BY 2 LOOP
		RAISE NOTICE 'COUNTER: %', counter;
	END LOOP;
END; $$

-- 04
DO $$
BEGIN
	FOR counter IN REVERSE 6..1 BY 2 LOOP
		RAISE NOTICE 'COUNTER: %', counter;
	END LOOP;
END; $$

/*
    FOR LOOP USING FOR LOOPING THROUGH THE RESULT OF QUERY
    -------
    <<label>>
    FOR target IN query LOOP
        statement
    END LOOP;
    -------
*/

CREATE OR REPLACE FUNCTION for_loop_through_query(n INTEGER) 
RETURNS VOID
AS $$
DECLARE 
	rec RECORD;
BEGIN
	FOR rec IN (SELECT title FROM film ORDER BY title LIMIT n) LOOP
		RAISE NOTICE '%', rec.title;
	END LOOP;
END; $$
LANGUAGE PLPGSQL;

/*
    FOR LOOP USING FOR LOOPING THROUHG DYNAMIC QUERY
    -------
    <<label>>
    FOR target IN EXECUTE expression [USING a, b, c, ...] LOOP
        expression;
    END LOOP;
    -------
*/
CREATE OR REPLACE FUNCTION for_loop_through_dyn_query(sort_type INTEGER, n INTEGER)
RETURNS VOID AS $$
DECLARE 
	rec RECORD;
	query TEXT;
BEGIN 
	
	query = 'SELECT release_year, title FROM film ';
	
	IF sort_type = 1 THEN
		query := query || 'ORDER BY title';
	ELSIF sort_type = 2 THEN
	  	query := query || 'ORDER BY release_year';
	ELSE 
		RAISE EXCEPTION 'Invalid sort type %s', sort_type;
	END IF;
	
	query = query || 'LIMITS $1';
	
	FOR rec IN EXECUTE query USING n LOOP
		RAISE NOTICE '% - %', rec.release_year, title;
	END LOOP;
END; $$
LANGUAGE PLPGSQL;