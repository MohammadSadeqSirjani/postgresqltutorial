# PL/pgSQL Constants

### Summary: 
* In this tutorial, we will show you how to declare and use PL/pgSQL constants.

Unlike [variables](https://www.postgresqltutorial.com/plpgsql-variables/), the values of constants cannot be changed once they are initialized. The following are reasons to use constants.

First, the constants make the code more readable e.g., imagine that we have a formula as follows:
```
selling_price = net_price + net_price * 0.1;
```
What does 0.1 means? It can be interpreted to anything. But, when we use the following formula, we know the meaning of the calculation of the selling price that equals to net price plus value-added tax (VAT).
```
selling_price = net_price + net_price * VAT;
```
Let’s examine each component of the syntax in more detail:

* First, specify the constant name. By convention, it is in the uppercase form such as VAT and  DISCOUNT.
* Second, put the CONSTANT keyword and specify the [data type](https://www.postgresqltutorial.com/postgresql-data-types/) that the constant is associated with.
Third, initialize a value for the constant.

# PL/pgSQL constants example
The following example declares a constant named VAT for valued added tax and calculates the selling price from the net price:

```
DO $$ 
DECLARE
   VAT CONSTANT NUMERIC := 0.1;
   net_price    NUMERIC := 20.5;
BEGIN 
   RAISE NOTICE 'The selling price is %', net_price * ( 1 + VAT );
END $$;
```
```
NOTICE:  The selling price is 22.55
```
Now, if you try to change the value of the constant as follows:
```
DO $$ 
DECLARE
   VAT constant NUMERIC := 0.1;
   net_price    NUMERIC := 20.5;
BEGIN 
   RAISE NOTICE 'The selling price is %', net_price * ( 1 + VAT );
   VAT := 0.05;
END $$;
```
You will get an error message:
```
ERROR: "vat" is declared CONSTANT
SQL state: 22005
Character: 155
```
Notice that PostgreSQL evaluates the value for the constant when the block is entered at run-time, not compile-time. Consider the following example:
```
DO $$ 
DECLARE
   start_at CONSTANT time := now();
BEGIN 
   RAISE NOTICE 'Start executing block at %', start_at;
END $$;
```
```
NOTICE:  Start executing block at 17:49:59.791
```
PostgreSQL evaluates the NOW() function every time we call the block. To prove it, we execute the block again:
```
NOTICE:  Start executing block at 17:50:44.956
```
And got a different result.

In this tutorial, you have learned how to declare and use constants in PL/pgSQL.