DO $$
DECLARE
	created_time TIME = CURRENT_TIME;
BEGIN
	RAISE NOTICE '%', created_time;
	
	PERFORM pg_sleep(10);

	RAISE NOTICE '%', created_time;
END	$$;	