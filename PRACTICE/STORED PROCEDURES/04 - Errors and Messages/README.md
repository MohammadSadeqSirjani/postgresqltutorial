# PL/pgSQL Errors and Messages

### Summary: 
* In this tutorial, we will show you how to report messages and raise errors using the RAISE statement. In addition, we will introduce you to the ASSERT statement for inserting debugging checks into PL/pgSQL blocks.

# PL/pgSQL reporting messages
To raise a message, you use the RAISE statement as follows:
```
RAISE level format;
```
Let’s examine the components of the ```RAISE``` statement in more detail.

Following the ```RAISE``` statement is the ```level``` option that specifies the error severity. PostgreSQL provides the following levels:

 * DEBUG
 * LOG
 * NOTICE
 * INFO
 * WARNING
 * EXCEPTION

If you don’t specify the ```level```, by default, the ```RAISE``` statement will use ```EXCEPTION``` level that raises an error and stops the current transaction. We will discuss the ```RAISE EXCEPTION``` later in the next section.

The ```format``` is a string that specifies the message. The ```format``` uses percentage (`%`) placeholders that will be substituted by the next arguments. The number of placeholders must match the number of arguments, otherwise, PostgreSQL will report the following error message:
```
[Err] ERROR:  too many parameters specified for RAISE
```
The following example illustrates the RAISE statement that reports different messages at the current time.
```
DO $$ 
BEGIN 
  RAISE INFO 'information message %', now() ;
  RAISE LOG 'log message %', now();
  RAISE DEBUG 'debug message %', now();
  RAISE WARNING 'warning message %', now();
  RAISE NOTICE 'notice message %', now();
END $$;
```
```
INFO:  information message 2015-09-10 21:17:39.398+07
WARNING:  warning message 2015-09-10 21:17:39.398+07
NOTICE:  notice message 2015-09-10 21:17:39.398+07
```
Notice that not all messages are reported back to the client, only ```INFO```, ```WARNING```, and ```NOTICE``` level messages are reported to the client. This is controlled by the ```client_min_messages``` and ```log_min_messages``` configuration parameters.

# PL/pgSQL raising errors
To raise errors, you use the ```EXCEPTION``` level after the ```RAISE``` statement. Note that ```RAISE``` statement uses the ```EXCEPTION``` level by default.

Besides raising an error, you can add more detailed information by using the following clause with the ```RAISE``` statement:
```
USING option = expression
```
The option can be:

 * MESSAGE: set error message text
 * HINT: provide the hint message so that the root cause of the error is easier to be discovered.
 * DETAIL:  give detailed information about the error.
 * ERRCODE: identify the error code, which can be either by condition name or directly five-character SQLSTATE code. Please refer to the table of error codes and condition names.
The expression is a string-valued expression.

The following example raises a duplicate email error message:
```
DO $$ 
DECLARE
  email varchar(255) := 'info@postgresqltutorial.com';
BEGIN 
  -- check email for duplicate
  -- ...
  -- report duplicate email
  RAISE EXCEPTION 'Duplicate email: %', email 
		USING HINT = 'Check the email again';
END $$;
```
```
[Err] ERROR:  Duplicate email: info@postgresqltutorial.com
HINT:  Check the email again
```
The following examples illustrate how to raise an SQLSTATE and its corresponding condition:
```
DO $$ 
BEGIN 
	--...
	RAISE SQLSTATE '2210B';
END $$;
```
```
DO $$ 
BEGIN 
	--...
	RAISE invalid_regular_expression;
END $$;
```

# PL/pgSQL putting debugging checks using ASSERT statement
~~~
Notice that PostgreSQL introduces the ASSERT statement since version 9.5. Check your PostgreSQL version before using it.
~~~
Sometimes, a [PL/pgSQL function](https://www.postgresqltutorial.com/postgresql-create-function/) is so big that makes it more difficult to detect the bugs. To facilitate this, PostgreSQL provides you with the ```ASSERT``` statement for adding debugging checks into a PL/pgSQL function.

The following illustrates the syntax of the ```ASSERT``` statement:
```
ASSERT condition [, message];
```
The condition is a boolean expression. If the condition evaluates to ```TRUE```, ```ASSERT``` statement does nothing. If the condition evaluates to ```FALSE``` or ```NULL```, the ```ASSERT_FAILURE``` is raised.

If you don’t provide the ```message```, PL/pgSQL uses ” assertion failed” message by default. If the message is provided, the ASSERT statement will use it to replace the default ```message```.
```
DO $$ 
DECLARE 
   counter integer := -1;
BEGIN 
   ASSERT counter = 0, 'Expect counter starts with 0';
END $$;
```
It is important to note that the ```ASSERT``` statement is used for debugging only.

Now you can use ```RAISE``` statement to either raise a message or report an error.
