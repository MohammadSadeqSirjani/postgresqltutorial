CREATE OR REPLACE FUNCTION get_film (p_pattern VARCHAR) 
	RETURNS TABLE (
		film_title VARCHAR,
		film_release_year INT
) 
AS $$
BEGIN
	RETURN QUERY SELECT
		title,
		cast( release_year as integer)
	FROM
		film
	WHERE
		title ILIKE p_pattern ;
END; $$ 

LANGUAGE 'plpgsql';