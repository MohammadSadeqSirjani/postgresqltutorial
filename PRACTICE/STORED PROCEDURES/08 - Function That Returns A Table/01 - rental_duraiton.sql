/*
    (RETURN NEXT) IS SLOWER THAN (RETURN QUERY) IN NORMAL DATABASE
    (RETURN NEXT) IS USE LOW MEMORY USAGE THAN (RETURN QUERY)
    IN BIG DATA FOR COMPLETE RETRIEVING DATE FROM BIG TABLE, SHOULF PREFER TO USE (RETURN NEXT)
*/
CREATE OR REPLACE FUNCTION "public"."rental_duration"()
  RETURNS TABLE("store_rental_id" int4, "store_customer_id" int2, "store_borrow_time" int4) AS $BODY$
DECLARE var_r RECORD;
BEGIN
	FOR var_r IN(SELECT rental_id, customer_id, return_date, rental_date FROM rental)LOOP
		store_rental_id = var_r.rental_id;
		store_customer_id = var_r.customer_id;
		store_borrow_time = SUM(EXTRACT(DAY FROM (var_r.return_date - var_r.rental_date)));
		RETURN NEXT;
	END	 LOOP;
END; $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000

CREATE OR REPLACE FUNCTION "public"."rental_duration_v2"()
  RETURNS TABLE("store_rental_id" int4, "store_customer_id" int2, "store_borrow_time" int4) AS $BODY$
BEGIN
	RETURN QUERY
	SELECT 
	rental_id,
	customer_id,
	SUM(EXTRACT(DAY FROM (return_date - rental_date)))::INTEGER 
	FROM rental
	GROUP BY rental_id, customer_id;
	RETURN;	
END; $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000