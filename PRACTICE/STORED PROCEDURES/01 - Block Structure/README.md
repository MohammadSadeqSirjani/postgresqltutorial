# PL/pgSQL Block Structure

## Summary:
* In this tutorial, you will learn about the block structure of PL/pgSQL. You will write and execute the first PL/pgSQL block.

PL/pgSQL is a block-structured language, therefore, a PL/pgSQL function or stored procedure is organized into blocks.

The following illustrates the syntax of a complete block in PL/pgSQL:

```
[ <<label>> ]
[ DECLARE
    declarations ]
BEGIN
    statements;
	...
END [ label ];
```
Let’s examine the block structure in more detail:

* Each block has two sections: declaration and body. The declaration section is optional while the body section is required. The block is ended with a semicolon (;) after the END keyword.
* A block may have an optional label located at the beginning and at the end. You use the block label in case you want to specify it in the EXIT statement of the block body or if you want to qualify the names of variables declared in the block.
* The declaration section is where you declare all variables used within the body section. Each statement in the declaration section is terminated with a semicolon (;).
* The body section is where you place the code. Each statement in the body section is also terminated with a semicolon (;).

# PL/pgSQL block structure example

The following example illustrates a very simple block. It is called an anonymous block.

```
DO $$ 
<<first_block>>
DECLARE
  counter integer := 0;
BEGIN 
   counter := counter + 1;
   RAISE NOTICE 'The current value of counter is %', counter;
END first_block $$;
```
```NOTICE:  The current value of counter is 1```

To execute this block from pgAdmin, you click the Execute button as shown in the following picture:
![alt](./images/PLpgSQL-Block-Structure.png)

Notice that the DO statement does not belong to the block. It is used to execute an anonymous block. PostgreSQL introduced the DO statement since version 9.0.

In the declaration section, we declared a [variable](https://www.postgresqltutorial.com/plpgsql-variables/) counter and set its value to zero.

Inside the body section, we increased the value of the counter to one and output its value using [RAISE NOTICE](https://www.postgresqltutorial.com/plpgsql-errors-messages/) statement.

The ```first_block``` label is just for demonstration purpose. It does nothing in this example.

# What is the double dollar ($$)?
The double dollar ($$) is a substitution of a single quote (‘).  When you develop a PL/pgSQL block, a function, or a stored procedure, you have to pass its body in the form of a string literal. In addition, you have to escape all single quote (‘) in the body as follows:

```
DO
'<<first_block>>
DECLARE
  counter integer := 0;
BEGIN 
  
    counter := counter + 1;
    RAISE NOTICE ''The current value of counter is %'', counter;

END first_block';
```
If you use the double dollar ($$) you can avoid quoting issues. You can also use a token between $$ like  $function$ or $procedure$.

# PL/pgSQL Sub-block
PL/pgSQL allows you to place a block inside the body of another block. This block nested inside another block is called sub-block. The block that contains the sub-block is referred to as an outer block.

![alt](./images/plpgsql-block-structure_1.png)

The sub-blocks are used for grouping statements so that a large block can be divided into smaller and more logical sub-blocks. The variables in the sub-block can have the names as the ones in the outer block, even though it is not a good practice.

When you declare a variable within sub-block with the same name as the one in the outer block, the variable in the outer block is hidden in the sub-block. In case you want to access a variable in the outer block, you use block label to qualify its name as shown in the following example:

```
DO $$ 
<<outer_block>>
DECLARE
  counter integer := 0;
BEGIN 
   counter := counter + 1;
   RAISE NOTICE 'The current value of counter is %', counter;

   DECLARE 
       counter integer := 0;
   BEGIN 
       counter := counter + 10;
       RAISE NOTICE 'The current value of counter in the sub-block is %', counter;
       RAISE NOTICE 'The current value of counter in the outer block is %', outer_block.counter;
   END;

   RAISE NOTICE 'The current value of counter in the outer block is %', counter;
   
END outer_block $$;
```

```
NOTICE:  The current value of counter is 1
NOTICE:  The current value of counter in the sub-block is 10
NOTICE:  The current value of counter in the outer block is 1
NOTICE:  The current value of counter in the outer block is 1
```
In this example:

* First, we declared a variable named counter in the outer_block.
* Next, in the sub-block, we also declared a variable with the same name.
* Then, before entering into the sub*block, the value of the counter is one. In the sub-block, we increased the value of the counter to ten and print it out. Notice that the change only affects the counter variable in the sub-block.
* After that, we referred to the counter variable in the outer block using the block label to qualify its name outer_block.counter.
* Finally, we printed out the value of the counter variable in the outer block, its value remains intact.

In this tutorial, you have learned about the PL/pgSQL block structure and how to execute an anonymous block using the DO statement.