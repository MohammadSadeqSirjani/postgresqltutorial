DO $$
<<outer_block>>
DECLARE counter INTEGER = 0;
BEGIN
	counter = counter + 100;
	RAISE NOTICE 'THE RESULT OF OUTER_BLOCK COUNTER IS %', counter;
	
	
	DECLARE counter INTEGER = 0;
	BEGIN 
		counter = counter + outer_block.counter;
		
		RAISE NOTICE 'THE RESULT OF SUB_BLOCK COUNTER IS %', counter;
		RAISE NOTICE 'THE RESULT OF OUTER_BLOCK COUNTER IS %', outer_block.counter;
	END;
	
	RAISE NOTICE 'THE RESULT OF OUTER_BLOCK COUNTET IS %', outer_block.counter;
END OUTER_BLOCK $$;	