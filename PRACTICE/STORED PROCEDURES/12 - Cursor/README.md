# PL/pgSQL Cursor

### Summery
* In this tutorial, we will show you how to use PL/pgSQL Cursor and give you some practical examples of using cursors.

## Definition
A PL/pgSQL cursor allows us to encapsulate a query and process each individual row at a time. We use cursors when we want to divide a large result set into parts and process each part individually. If we process it at once, we may have a memory overflow error.

In addition, we can [develop a function](https://www.postgresqltutorial.com/postgresql-create-function/) that returns a reference to a cursor. This is an efficient way to return a large result set from a function. The caller of the function can process the result set based on the cursor reference.

The following diagram illustrates how to use a cursor in PostgreSQL:

![alt](./images/plpgsql-cursor.png)

* First, declare a cursor.
* Next, open the cursor.
* Then, fetch rows from the result set into a target.
* After that, check if there is more row left to fetch. If yes, go to step 3, otherwise, go to step 5.
* Finally, close the cursor.

## Declaring cursors
To access to a cursor, you need to declare a cursor [variable link](https://www.postgresqltutorial.com/plpgsql-variables/) in the [declaration section of a block link](https://www.postgresqltutorial.com/plpgsql-block-structure/). PostgreSQL provides us with a special type called 'REFCURSOR' to declare a cursor variable.

```
DECLARE 
   my_cursor REFCURSOR;
```
Another way to declare a cursor that bounds to a query are using the following syntax:

```
cursor_name [ [NO] SCROLL ] CURSOR [( name datatype, name data type, ...)] FOR query;
```
First, you specify a variable name for the cursor.
Next, you specify whether the cursor can be scrolled backward using the SCROLL. If you use NO SCROLL, the cursor cannot be scrolled backward.
Then, you put the CURSOR keyword followed by a list of comma-separated arguments (```name datatype```) that defines parameters for the query. These arguments will be substituted by values when the cursor is opened.
After that, you specify a query following the FOR keyword. You can use any valid SELECT statement here.

The following example illustrates how to declare cursors:

```
DECLARE
    cur_films  CURSOR FOR SELECT * FROM film;
    cur_films2 CURSOR (year integer) FOR SELECT * FROM film WHERE release_year = year;
```

The cur_films is a cursor that encapsulates all rows in the film table.
The cur_films2 is a cursor that encapsulates film with a particular release year in the film table.

