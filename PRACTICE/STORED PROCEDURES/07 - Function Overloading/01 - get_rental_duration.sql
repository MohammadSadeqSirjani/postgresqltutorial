CREATE OR REPLACE FUNCTION get_rental_duration(p_customer_id INTEGER)
	RETURNS INTEGER AS $$
	
DECLARE 
	rental_duration INTEGER; 
BEGIN
	-- get the rate based on film_id
	SELECT INTO rental_duration SUM( EXTRACT( DAY FROM return_date - rental_date)) 
    FROM rental 
	WHERE customer_id=p_customer_id;

	RETURN rental_duration;
END; $$
LANGUAGE plpgsql;