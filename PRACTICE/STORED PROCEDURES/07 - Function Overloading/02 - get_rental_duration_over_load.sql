CREATE OR REPLACE FUNCTION get_rental_duration(p_customer_id INTEGER, p_from_date DATE)
	RETURNS INTEGER AS $$
DECLARE 
	rental_duration integer;
BEGIN
	-- get the rental duration based on customer_id and rental date
	SELECT INTO rental_duration
	            SUM( EXTRACT( DAY FROM return_date + '12:00:00' - rental_date)) 
	FROM rental 
	WHERE customer_id= p_customer_id AND 
		  rental_date >= p_from_date;
	 
	RETURN rental_duration;
END; $$
LANGUAGE plpgsql;