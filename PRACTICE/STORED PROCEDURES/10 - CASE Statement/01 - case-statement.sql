/*
    CASE STATMENT
*/

CREATE OR REPLACE FUNCTION get_price_segment(p_film_id INTEGER)
RETURNS VARCHAR(50) 
AS $$
DECLARE 
	rate NUMERIC;
	price_segment VARCHAR(50);
BEGIN
	SELECT INTO rate rental_rate
	FROM film
	WHERE film_id = p_film_id;
	
	CASE rate
	WHEN 0.99 THEN
		price_segment = 'Mass';
	WHEN 2.99 THEN
		price_segment = 'Mainstream';
	WHEN 4.99 THEN
		price_segment = 'High End';
	ELSE
		price_segment = 'Unspecified';
	END CASE;
	
	RETURN price_segment;
END; $$
LANGUAGE PLPGSQL;

/*
    SEARCHED CASE
    SEARCHED CASE STATEMENT IS SIMLAT TO IF/ELSIF/ELSE STATEMENT
*/

CREATE OR REPLACE FUNCTION get_customer_service(p_customer_id INTEGER) 
RETURNS VARCHAR(25)
AS $$ 
DECLARE
	total_payment NUMERIC;
	service_level VARCHAR(25);
BEGIN
	SELECT 
		INTO total_payment SUM(amount)
	FROM 
		payment
	WHERE
		customer_id = p_customer_id;
		
	CASE
	WHEN total_payment > 200 THEN
		service_level = 'Platinum';
	WHEN total_payment > 100 THEN
		service_level = 'Gold';	
	ELSE
		service_level = 'Silve';
	END CASE;
	
	RETURN service_level;
END; $$
LANGUAGE PLPGSQL;