SELECT 
	cu.customer_id AS id,
	CONCAT_WS(' ', cu.first_name, last_name) as name,
	a.address,
	a.postal_code as "Zip Code",
	a.phone,
	ct.city,
	ctr.country,
	CASE 
		WHEN cu.activebool THEN 'active'
		ELSE ''
	END AS notes	
FROM customer cu
INNER JOIN address a USING(address_id)
INNER JOIN city ct USING(city_id)
INNER JOIN country ctr USING(country_id)