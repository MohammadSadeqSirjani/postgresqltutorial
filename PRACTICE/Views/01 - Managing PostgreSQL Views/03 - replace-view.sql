CREATE OR REPLACE VIEW customer_master AS
SELECT 
	cu.customer_id AS id,
	CONCAT_WS(' ', cu.first_name, cu.last_name)	AS name,
	a.address,
	a.postal_code AS "Zip Code",
	a.phone,
	ct.city,
	ctr.country,
	CASE
		WHEN cu.activebool THEN 'active'
		ELSE ''
	END AS notes,	
	cu.store_id AS sid,
	cu.email
FROM	
	customer cu 
	INNER JOIN address a USING(address_id)
	INNER JOIN city ct USING(city_id)
	INNER JOIN country ctr USING(country_id)