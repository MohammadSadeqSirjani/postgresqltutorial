# Creating PostgreSQL Updatable Views

### Summary: 
* In this tutorial, we will discuss the requirements for updatable views and show you how to create updatable views in PostgreSQL.

A PostgreSQL view is updatable when it meets the following conditions:
* The defining query of the view must have exactly one entry in the FROM clause, which can be a table or another updatable view.
* The defining query must not contain one of the following clauses at the top level: [```GROUP BY```](https://www.postgresqltutorial.com/postgresql-group-by/), [```HAVING```](https://www.postgresqltutorial.com/postgresql-having/), [```LIMIT```](https://www.postgresqltutorial.com/postgresql-limit/), ```OFFSET```, [```DISTINCT```](https://www.postgresqltutorial.com/postgresql-select-distinct/), ```WITH```, [```UNION```](https://www.postgresqltutorial.com/postgresql-union/), [```INTERSECT```](https://www.postgresqltutorial.com/postgresql-intersect/), and [```EXCEPT```](https://www.postgresqltutorial.com/postgresql-tutorial/postgresql-except/).
* The selection list must not contain any [```window function```](https://www.postgresqltutorial.com/postgresql-window-function/) , any [```set-returning function```](https://www.postgresqltutorial.com/plpgsql-function-returns-a-table/), or any [```aggregate function```](https://www.postgresqltutorial.com/postgresql-aggregate-functions/) such as [```SUM```](https://www.postgresqltutorial.com/postgresql-sum-function/), [```COUNT```(https://www.postgresqltutorial.com/postgresql-count-function/), [```AVG```](https://www.postgresqltutorial.com/postgresql-avg-function/), [```MIN```](https://www.postgresqltutorial.com/postgresql-min-function/), and [```MAX```](https://www.postgresqltutorial.com/postgresql-max-function/).
    